/*
1. Цикли потрібні для того, щоб виконувати n кількість заданих операцій, без ручного втручання.
2. Я використовував цикл, наприклад, коли мені потрібно було перебрати числа від 1 до 10 і виконати з кожним числом певну дію. Або коли мені треба виконати певну дію з кожним елементом масива чи об'єкта.
3. Явне перетворення типів - це коли ми використовуємо зарезервовані у JavaScript функції Number(), String(), Boolean().
Неявне перетворення типів - це коли при арифметичних діях (*, /, +, -, і тд.) між різними типами операндів ми зводимо їх до одного типу.
*/

//

/* 
let userNumber = +prompt('Введіть ціле число:')

while (userNumber % 1 != 0) {
  userNumber = +prompt('Введіть ціле число:')
}

let isFlag = false

repeat: for (let i = 0; i <= userNumber; i++) {
  if (i === 0) {
    continue repeat
  } else if (i % 5 === 0) {
    isFlag = true
    console.log(i)
  } else if (isFlag === false && i === userNumber) {
    console.log('Sorry, no numbers')
  }
} 
*/

//

let m = +prompt('Введіть число m')
let n = +prompt('Введіть число n')

let min = Math.min(m, n)
let max = Math.max(m, n)

while (m % 1 != 0 || n % 1 != 0) {
  alert('Error')
  m = +prompt('Введіть число m')
  n = +prompt('Введіть число n')
}

repeat: for (let i = min; i <= max; i++) {
  for (let y = 2; y < i; y++) {
    if (i % y === 0) continue repeat
  }

  if (i > 1) console.log(i)
}
